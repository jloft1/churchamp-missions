<?php
/**
 * Plugin Name: 	Endeavr / ChurchAmp - Missions Module
 * Plugin URI: 	http://churchamp.com/plugins/missions
 * Description: 	A ChurchAmp Feature Module. [Missions]
 * Version: 		5.0.0
 * Author: 		Endeavr Media (Jason Loftis / jLoft)
 * Author URI: 	http://endeavr.com
 *
 * Notes: By default, this version of the plugin rewrites the permalinks as follows...
 * post-type archive (missionaries cpt)		=>	/missions/missionaries/
 * post-type single (missionaries cpt) 		=>	/missions/missionaries/missionary-name
 * taxonomy missionagency 				=>	/missions/agency/
 * taxonomy missioncountry 				=>	/missions/country/
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @package		ChurchAmp_Missions
 * @version		5.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/missions
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

class ChurchAmp_Missions {

	/**
	 * PHP5 constructor method.
	 *
	 * @since  5.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		/* Set the constants needed by the plugin. */
		add_action( 'plugins_loaded', array( &$this, 'constants' ), 1 );
		/* Load the functions files. */
		add_action( 'plugins_loaded', array( &$this, 'includes' ), 2 );
		/* Load the admin stylesheets. */
		add_action( 'plugins_loaded', array( &$this, 'styles' ), 3 );
		/* Load the admin files. */
		//add_action( 'plugins_loaded', array( &$this, 'admin' ), 4 );
		/* Register activation hook. */
		register_activation_hook( __FILE__, array( &$this, 'activation' ) );
	}

	/**
	 * Defines constants used by the plugin.
	 *
	 * @since  5.0.0
	 * @access public
	 * @return void
	 */
	public function constants() {
		/* Set constant path to the plugin directory. */
		define( 'ENDVR_CA_MISSIONS_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		/* Set the constant path to the plugin directory URI. */
		define( 'ENDVR_CA_MISSIONS_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );
		/* Set the constant path to the includes directory. */
		define( 'ENDVR_CA_MISSIONS_INCLUDES', ENDVR_CA_MISSIONS_DIR . trailingslashit( 'includes' ) );
		/* Set the constant path to the admin directory. */
		define( 'ENDVR_CA_MISSIONS_STYLES', ENDVR_CA_MISSIONS_URI . trailingslashit( 'styles' ) );
		/* Set the constant path to the admin directory. */
		define( 'ENDVR_CA_MISSIONS_ADMIN', ENDVR_CA_MISSIONS_DIR . trailingslashit( 'admin' ) );
	}

	/**
	 * Loads the initial files needed by the plugin. (cpt => custom post type, tax => taxonomy, mb => meta box)
	 * It's important to register the taxonomies before the post type in order for the rewrite rules to work properly.
	 *
	 * @since  5.0.0
	 * @access public
	 * @return void
	 */
	public function includes() {
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'tax-mission-country.php' );
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'tax-mission-agency.php' );
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'cpt-missionaries.php' );
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'mb-missionary-details.php' );
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'mb-missionary-news.php' );
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'mb-missionary-photos.php' );
		require_once( ENDVR_CA_MISSIONS_INCLUDES . 'fn-missionaries.php' );
	}

	/**
	 * Loads the admin stylesheets.
	 *
	 * @since  5.0.0
	 * @access public
	 * @return void
	 */
	public function styles() {
		/* Load the translation of the plugin. */
		add_action('admin_head', 'endvr_admin_head_style_missions');
		function endvr_admin_head_style_missions() {
			echo '<link rel="stylesheet" type="text/css" href="'.ENDVR_CA_MISSIONS_STYLES.'style-missions.css">';
		}
	}

	/**
	 * Loads the admin functions and files.
	 *
	 * @since  5.0.0
	 * @access public
	 * @return void
	 */
	public function admin() {

		if ( is_admin() )
			require_once( ENDVR_CA_MISSIONS_ADMIN . 'options.php' );
	}

	/**
	 * Method that runs only when the plugin is activated.
	 *
	 * @since  5.0.0
	 * @access public
	 * @return void
	 * @example: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
	 */
	function activation() {

		/* Get the administrator role. */
		$role =& get_role( 'administrator' );

		/* If the administrator role exists, add required capabilities for the plugin. */
		if ( !empty( $role ) ) {

			$role->add_cap( 'manage_missions' );
			$role->add_cap( 'create_missions' );
			$role->add_cap( 'edit_missions' );
		}

		/* Flush rewrite rules upon activation. */
		flush_rewrite_rules();

	}

}

new ChurchAmp_Missions();