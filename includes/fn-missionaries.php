<?php
/**
 * Functions File for Missionaries CPT
 *
 * @package  		ChurchAmp_Missions
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/missions
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @example		https://github.com/justintadlock/custom-content-portfolio/blob/master/includes/functions.php
 */

/**
 * Filter to ensure the proper labels display when user updates custom post type entries.
 * translators: %s: date and time of the revision
 * translators: Publish box date format, @example: http://php.net/date
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */
add_filter( 'post_updated_messages', 'endvr_updated_messages_missionaries' );
function endvr_updated_messages_missionaries( $messages ) {
global $post, $post_ID;
	$messages['missionaries'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( __('Missionary Updated. <a href="%s">View Missionary</a>'), esc_url( get_permalink($post_ID) ) ),
		2  => __('Missionary Updated.'),
		3  => __('Missionary Deleted.'),
		4  => __('Missionary Updated.'),
		5  => isset($_GET['revision']) ? sprintf( __('Missionary Restored to Revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( __('Missionary Published. <a href="%s">View Missionary</a>'), esc_url( get_permalink($post_ID) ) ),
		7  => __('Missionary Saved.'),
		8  => sprintf( __('Missionary Submitted. <a target="_blank" href="%s">Preview Missionary</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9  => sprintf( __('Missionary Scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Missionary</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10  => sprintf( __('Missionary Draft Updated. <a target="_blank" href="%s">Preview Missionary</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}

/**
 * Filter on 'post_type_archive_title' to allow for the use of the 'archive_title' label.
 * Not supported by WordPress natively, but that's okay since we can roll our own labels.
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */

add_filter( 'post_type_archive_title', 'endvr_post_type_archive_title_missionaries' );
function endvr_post_type_archive_title_missionaries( $title ) {
	if ( is_post_type_archive( 'missionaries' ) ) {
		$post_type = get_post_type_object( 'missionaries' );
		$title = isset( $post_type->labels->archive_title ) ? $post_type->labels->archive_title : $title;
	}
	return $title;
}

/**
 * Filter to assign custom text instructions on publish form to replace "Enter title here".
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */

add_filter( 'enter_title_here', 'endvr_replace_title_label_missionaries' );
function endvr_replace_title_label_missionaries( $title ) {
	$screen = get_current_screen();
	if  ( 'missionaries' == $screen->post_type ) {
		$title = 'Enter Missionary Name Here (Last, First)...';
		return $title;
	}
}

/**
 * Assign a custom label to the Sub Title.
 *
 * @since  0.1.0
 * @access public
 * @example: http://codex.wordpress.org/Function_Reference/remove_filter
 */

add_action( 'admin_head', 'endvr_replace_subtitle_label_missionaries' );
function endvr_subtitle_label_missionaries() {
	echo 'Input a Subtitle for this Missionary (Optional)';
}
function endvr_replace_subtitle_label_missionaries() {
	$screen = get_current_screen();
	if  ( 'missionaries' == $screen->post_type ) {
		remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
		add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_missionaries' );
	}
}

/**
 * Assign a custom label to the Post Editor + place it in a sortable Meta Box.
 *
 * @since  0.1.0
 * @access public
 * @example: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
 * @example: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
 */

add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_missionaries', 0 );
function endvr_add_meta_box_editor_missionaries() {
	$screen = get_current_screen();
	if  ( 'missionaries' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_missionaries',
					__('Missionary Profile'),
					'endvr_meta_box_editor_missionaries',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_missionaries' ); //white background
}
function endvr_admin_head_missionaries() {
?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
<?php
}
function endvr_meta_box_editor_missionaries( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

/**
 * Redefine the way the content type's admin panel index listing is displayed.
 *
 * @since  0.1.0
 * @access public
 * @example: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
 * @example: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/
 */

add_filter( 'manage_edit-missionaries_columns', 'endvr_set_custom_edit_columns_missionaries' );
add_action( 'manage_missionaries_posts_custom_column',  'endvr_custom_column_missionaries', 10, 2 );

function endvr_set_custom_edit_columns_missionaries($columns) {
	// unset($columns['date']); // we want to show the date for missionaries
	return $columns
		+ array(
		'_endvr_missionary_org' 			=> __('Organization'),
		'_endvr_missionary_role' 		=> __('Position'),
		'_endvr_missionary_email'		=> __('Email'),
		'_endvr_missionary_photo_thumb' 	=> __('Thumbnail'),
		'_endvr_missionary_photo_full' 	=> __('Full Photo'),
	);
}

function endvr_custom_column_missionaries($column, $post_id) {
	switch ( $column ) {
		case '_endvr_missionary_org':
			echo get_post_meta( $post_id , '_endvr_missionary_org' , true );
		break;
		case '_endvr_missionary_role':
			echo get_post_meta( $post_id , '_endvr_missionary_role' , true );
		break;
		case '_endvr_missionary_email':
			echo get_post_meta( $post_id , '_endvr_missionary_email' , true );
		break;
		case '_endvr_missionary_photo_thumb': ?>
			<img src="<?php echo get_post_meta( $post_id , '_endvr_missionary_photo_thumb' , true ); ?>" width="133">
		<?php
		break;
		case '_endvr_missionary_photo_full': ?>
			<img src="<?php echo get_post_meta( $post_id , '_endvr_missionary_photo_full' , true ); ?>"width="100">
		<?php
		break;
	}
}