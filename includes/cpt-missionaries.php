<?php
/**
 * Custom Post Type ( Register the Missionaries CPT )
 *
 * @package  		ChurchAmp_Missions
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/missions
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the custom content type on the 'init' hook */
/* @ example: http://codex.wordpress.org/Function_Reference/register_post_type */
add_action( 'init', 'endvr_register_cpt_missionaries' );
function endvr_register_cpt_missionaries()
{
	/* labels used when displaying the posts in the admin */
	$labels = array(
		'name'               	=> __( 'Missionaries',                   	'churchamp-missions' ),
		'singular_name'      	=> __( 'Missionary',                    	'churchamp-missions' ),
		'menu_name'          	=> __( 'Missionaries',                      	'churchamp-missions' ),
		'name_admin_bar'     	=> __( 'Missionaries',                    	'churchamp-missions' ),
		'add_new'            	=> __( 'Add New',                      		'churchamp-missions' ),
		'add_new_item'       	=> __( 'Add New Missionary',            	'churchamp-missions' ),
		'edit_item'          	=> __( 'Edit Missionary',               	'churchamp-missions' ),
		'new_item'           	=> __( 'New Missionary',                	'churchamp-missions' ),
		'view_item'          	=> __( 'View Missionary',               	'churchamp-missions' ),
		'search_items'       	=> __( 'Search Missionaries',               	'churchamp-missions' ),
		'not_found'          	=> __( 'No Missionaries Found',          	'churchamp-missions' ),
		'not_found_in_trash' 	=> __( 'No Missionaries Found in Trash', 	'churchamp-missions' ),
		'all_items'          	=> __( 'Missionaries',                   	'churchamp-missions' ),
		'parent_item_colon'  	=> __( 'Parent Directory', 				'churchamp-missions' ),
		// custom labels because WordPress doesn't have anything to handle this
		'archive_title'		=> __( 'Missionaries',					'churchamp-missions' ),
	);
	$capability_type = array(
		'mission',
		'missions',
	);
	/* only 3 caps are needed: 'manage_missionaries', 'create_missionaries', 'edit_missionaries' */
	$capabilities = array(
		// meta caps (don't assign these to roles)
		'edit_post'     		=> 'edit_mission',
		'read_post'    		=> 'read_mission',
		'delete_post'   		=> 'delete_mission',
		// primitive/meta caps
		'create_posts'			=> 'create_missions',
		// primitive caps used outside of map_meta_cap()
		'edit_posts'   		=> 'edit_missions',
		'edit_others_posts'  	=> 'manage_missions',
		'publish_posts'  		=> 'manage_missions',
		'read_private_posts'  	=> 'read',
		// primitive caps used inside of map_meta_cap()
		'read'				=> 'read',
		'delete_posts'			=> 'manage_missions',
		'delete_private_posts' 	=> 'manage_missions',
		'delete_published_posts' => 'manage_missions',
		'delete_others_posts'	=> 'manage_missions',
		'edit_private_posts' 	=> 'edit_missions',
		'edit_published_posts' 	=> 'edit_missions',
	);
	$supports = array(
		'title',
		'editor',
	);
	$taxonomies = array(
		'missioncountry',
		'missionagency',
	);
	$rewrite = array(
		'slug'				=> 'missions/missionaries',
		'with_front'			=> false,
		'pages'				=> true,
		'feeds'				=> true,
		'ep_mask'				=> EP_PERMALINK,
	);
	$args = array(
		'description'			=> '',
		'public'    			=> true, 		// several of the other variables derive their values from 'public' by default
		'exclude_from_search'	=> false,
		'publicly_queryable' 	=> true,
		'can_export'  			=> true,
		'show_ui'    			=> true,
		'show_in_admin_bar' 	=> true,
		'show_in_nav_menus' 	=> true,
		'show_in_menu'   		=> true,
		'menu_position'  		=> 35, 		// should be an integer between 26 and 58 to appear between Comments and Appearance in admin menu
		'menu_icon'   			=> '', 		// keep this line because it adds the .menu-icon-missionaries class to the <li> HTML
		'hierarchical'   		=> false,		// make this true only if you want the content type to function like a page instead of a post
		'delete_with_user'		=> true,		// @source: https://github.com/justintadlock/custom-content-portfolio/
		'has_archive'   		=> 'missions/missionaries', 	// must be explicitly set in order for rewrite rules to work
		'query_var'   			=> 'missionary',	// should be singular
		'map_meta_cap'  		=> true, 		// this should be set to false if you wish to set up custom mapping of meta capabilities
		'capability_type'  		=> $capability_type,
		'capabilities'  		=> $capabilities,
		'labels'    			=> $labels,
		'supports'   			=> $supports,
		'taxonomies'   		=> $taxonomies,
		'rewrite'    			=> $rewrite,
	);
	register_post_type( 'missionaries', $args );
}