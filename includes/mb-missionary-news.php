<?php
/**
 * Meta Boxes ( Register Field Groups for Missionaries CPT )
 *
 * @package  		ChurchAmp_Missions
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/missions
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Meta Boxes require the Advanced Custom Fields plugin be activated.
 *
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => '5137de0860dbd',
		'title' => 'Missionary News',
		'fields' =>
		array (
			0 =>
			array (
				'key' => '_endvr_missionary_newsletters',
				'label' => 'Missionary Newsletters',
				'name' => '_endvr_missionary_newsletters',
				'type' => 'repeater',
				'order_no' => 0,
				'instructions' => 'Use these form fields to add a newsletter to this missionary profile. Additional newsletters may be included by clicking the "Add Another Newsletter" at the bottom of this section.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
						),
					),
					'allorany' => 'all',
				),
				'sub_fields' =>
				array (
					'_endvr_missionary_news_date' =>
					array (
						'label' => 'Newsletter Date',
						'name' => '_endvr_missionary_news_date',
						'type' => 'text',
						'instructions' => 'Provide a date for this newsletter (i.e. 2012-03). It is important to use that date format since it will be used for sorting. The newsletter title will also use this date.',
						'column_width' => 20,
						'default_value' => '',
						'formatting' => 'none',
						'order_no' => 0,
						'key' => '_endvr_missionary_news_date',
					),
					'_endvr_missionary_news_link' =>
					array (
						'label' => 'Newsletter Hyperlink (URL)',
						'name' => '_endvr_missionary_news_link',
						'type' => 'text',
						'instructions' => 'Use this field only if you are linking to an external file hosted elsewhere. If the church has the document, then use the upload tool below to add the document to the web server.',
						'column_width' => 40,
						'default_value' => '',
						'formatting' => 'none',
						'order_no' => 1,
						'key' => '_endvr_missionary_news_link',
					),
					'_endvr_missionary_news_doc' =>
					array (
						'label' => 'Newsletter PDF',
						'name' => '_endvr_missionary_news_doc',
						'type' => 'file',
						'instructions' => 'This newsletter document file will be inserted into the missionary profile as a link for download.',
						'column_width' => 40,
						'save_format' => 'url',
						'order_no' => 2,
						'key' => '_endvr_missionary_news_doc',
					),
				),
				'row_min' => 1,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Another Newsletter',
			),
		),
		'location' =>
		array (
			'rules' =>
			array (
				0 =>
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'missionaries',
					'order_no' => 2,
				),
			),
			'allorany' => 'all',
		),
		'options' =>
		array (
			'position' => 'advanced',
			'layout' => 'default',
			'hide_on_screen' =>
			array (
			),
		),
		'menu_order' => 2,
	));
}