<?php
/**
 * Meta Boxes ( Register Field Groups for Missionaries CPT )
 *
 * @package  		ChurchAmp_Missions
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/missions
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Meta Boxes require the Advanced Custom Fields plugin and its Repeater add-on be activated.
 *
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => '5137cafb23953',
		'title' => 'Missionary Details',
		'fields' =>
		array (
			0 =>
			array (
				'key' => '_endvr_missionary_org',
				'label' => 'Mission Organization',
				'name' => '_endvr_missionary_org',
				'type' => 'text',
				'order_no' => 0,
				'instructions' => 'Primary organization with whom the missionary is affiliated.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			1 =>
			array (
				'key' => '_endvr_missionary_website_org',
				'label' => 'Organization Website',
				'name' => '_endvr_missionary_website_org',
				'type' => 'text',
				'order_no' => 1,
				'instructions' => 'Primary personal website URL (format => http://missionary-organization.org).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			2 =>
			array (
				'key' => '_endvr_missionary_role',
				'label' => 'Role/Position Title',
				'name' => '_endvr_missionary_role',
				'type' => 'text',
				'order_no' => 2,
				'instructions' => 'If applicable, include the official title this missionary holds with his/her organization (i.e. Global Outreach Partners).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			3 =>
			array (
				'key' => '_endvr_missionary_project',
				'label' => 'Mission Project',
				'name' => '_endvr_missionary_project',
				'type' => 'text',
				'order_no' => 3,
				'instructions' => 'If applicable, include the name of the specific project to which this missionary belongs (i.e. Hope Mountain).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			4 =>
			array (
				'key' => '_endvr_missionary_website_project',
				'label' => 'Project Website',
				'name' => '_endvr_missionary_website_project',
				'type' => 'text',
				'order_no' => 4,
				'instructions' => 'If applicable, include the website URL for the mission project (format => http://missionary-project.org).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			5 =>
			array (
				'key' => '_endvr_missionary_location',
				'label' => 'Service Location',
				'name' => '_endvr_missionary_location',
				'type' => 'text',
				'order_no' => 5,
				'instructions' => 'Primary location where this missionary serves (format => City, Region, Country => i.e. Davao City, Mindanao, Philippines). The country the missionary serves in should also be specified using the taxonomy in the right sidebar. The taxonomy is used for organizational purposes.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			6 =>
			array (
				'key' => '_endvr_missionary_people_group',
				'label' => 'People Group Served',
				'name' => '_endvr_missionary_people_group',
				'type' => 'text',
				'order_no' => 6,
				'instructions' => 'Primary people group this missionary serves (i.e. Filipino pastors and leaders (tribal and city).).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			7 =>
			array (
				'key' => '_endvr_missionary_ministry_focus',
				'label' => 'Ministry Focus',
				'name' => '_endvr_missionary_ministry_focus',
				'type' => 'text',
				'order_no' => 7,
				'instructions' => 'Brief summation of this missionary\'s primary ministry responsibilities (i.e. Serving in leadership development and mentoring.).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'html',
			),
			8 =>
			array (
				'key' => '_endvr_missionary_prayer_focus',
				'label' => 'Prayer Focus',
				'name' => '_endvr_missionary_prayer_focus',
				'type' => 'text',
				'order_no' => 8,
				'instructions' => 'Brief summation of this missionary\'s general prayer need (i.e. Pray that Timothy and Gloria will stay strong in their walk with the Lord and that the Lord would give them wisdom and strength in the development of Hope Mountain. Pray also that the Lord would provide additional workers to serve at Hope Mountain.).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'html',
			),
			9 =>
			array (
				'key' => '_endvr_missionary_challenges',
				'label' => 'Unique Challenges',
				'name' => '_endvr_missionary_challenges',
				'type' => 'text',
				'order_no' => 9,
				'instructions' => 'Brief summation of the general challenges that are unique to this missionary\'s ministry (i.e. Mentoring first-time Filipino missionaries facing great challenges while serving in Asian countries who are very resistant to the gospel; challenging short-term workers to pursue goals for God’s glory.).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'html',
			),
			10 =>
			array (
				'key' => '_endvr_missionary_address_field',
				'label' => 'Postal Address (Mission Field)',
				'name' => '_endvr_missionary_address_field',
				'type' => 'text',
				'order_no' => 10,
				'instructions' => 'Input in this space the mailing address for the missionary while living on the field.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			11 =>
			array (
				'key' => '_endvr_missionary_address_home',
				'label' => 'Postal Address (Home/Permanent)',
				'name' => '_endvr_missionary_address_home',
				'type' => 'text',
				'order_no' => 11,
				'instructions' => 'If applicable, include the missionary\'s home/permanent address. This will most often be a US-based address.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			12 =>
			array (
				'key' => '_endvr_missionary_phone_field',
				'label' => 'Phone (Mission Field)',
				'name' => '_endvr_missionary_phone_field',
				'type' => 'text',
				'order_no' => 12,
				'instructions' => 'Primary mission field phone number. Be sure to start international phone numbers with 011 + the country code prefix + the phone number (format => 011+XXX-XXX-XXXX).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			13 =>
			array (
				'key' => '_endvr_missionary_phone_home',
				'label' => 'Phone (Home/Permanent)',
				'name' => '_endvr_missionary_phone_home',
				'type' => 'text',
				'order_no' => 13,
				'instructions' => 'If applicable, include the missionary\'s home/permanent phone number (format => XXX-XXX-XXXX).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			14 =>
			array (
				'key' => '_endvr_missionary_email',
				'label' => 'Email Address',
				'name' => '_endvr_missionary_email',
				'type' => 'text',
				'order_no' => 14,
				'instructions' => 'Primary email address (format => name@missiondomain.org).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			15 =>
			array (
				'key' => '_endvr_missionary_email_alt',
				'label' => 'Email Address (Alternate)',
				'name' => '_endvr_missionary_email_alt',
				'type' => 'text',
				'order_no' => 15,
				'instructions' => 'Alternate email address (format => name@missiondomain.org).',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			16 =>
			array (
				'key' => '_endvr_missionary_website_personal',
				'label' => 'Personal Website',
				'name' => '_endvr_missionary_website_personal',
				'type' => 'text',
				'order_no' => 16,
				'instructions' => 'Primary personal website URL (format => http://missionary-website.org). Often, the mission agency website maintains a profile page for the missionary.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			17 =>
			array (
				'key' => '_endvr_missionary_children',
				'label' => 'Children',
				'name' => '_endvr_missionary_children',
				'type' => 'text',
				'order_no' => 17,
				'instructions' => 'If applicable, list the names of this missionary family\'s children.',
				'required' => 0,
				'conditional_logic' =>
				array (
					'status' => 0,
					'rules' =>
					array (
						0 =>
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
		),
		'location' =>
		array (
			'rules' =>
			array (
				0 =>
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'missionaries',
					'order_no' => 1,
				),
			),
			'allorany' => 'all',
		),
		'options' =>
		array (
			'position' => 'advanced',
			'layout' => 'default',
			'hide_on_screen' =>
			array (
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'author',
				7 => 'format',
				8 => 'featured_image',
				9 => 'categories',
				10 => 'tags',
				11 => 'send-trackbacks',
			),
		),
		'menu_order' => 1,
	));
}