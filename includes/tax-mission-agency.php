<?php
/**
 * Taxonomy ( Register Mission Agency )
 *
 * @package  		ChurchAmp_Missions
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/missions
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_missionagency' );
function endvr_register_tax_missionagency() {

	$labels = array(
		'name'                       	=> __( 'Mission Agencies',                           		'churchamp-missions' ),
		'singular_name'              	=> __( 'Mission Agency',                            		'churchamp-missions' ),
		'menu_name'                  	=> __( 'Mission Agencies',                           		'churchamp-missions' ),
		'name_admin_bar'             	=> __( 'Mission Agencies',                            		'churchamp-missions' ),
		'search_items'               	=> __( 'Search '.'Mission Agencies'.'',                    	'churchamp-missions' ),
		'popular_items'              	=> __( 'Popular '.'Mission Agencies'.'',                   	'churchamp-missions' ),
		'all_items'                  	=> __( 'All '.'Mission Agencies'.'',                       	'churchamp-missions' ),
		'edit_item'                  	=> __( 'Edit '.'Mission Agency'.'',                       	'churchamp-missions' ),
		'view_item'                  	=> __( 'View '.'Mission Agency'.'',                       	'churchamp-missions' ),
		'update_item'                	=> __( 'Update '.'Mission Agency'.'',                     	'churchamp-missions' ),
		'add_new_item'               	=> __( 'Add New '.'Mission Agency'.'',                    	'churchamp-missions' ),
		'new_item_name'             	=> __( 'New '.'Mission Agency'.' Name',                	'churchamp-missions' ),
		'separate_items_with_commas' 	=> __( 'Separate '.'Mission Agencies'.' with Commas',      	'churchamp-missions' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.'Mission Agencies'.'',             	'churchamp-missions' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.'Mission Agencies'.'',	'churchamp-missions' ),
	);
	/* only 2 caps are needed: 'manage_missions' and 'edit_missions'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_missions',
		'edit_terms'   			=> 'manage_missions',
		'delete_terms' 			=> 'manage_missions',
		'assign_terms' 			=> 'edit_missions',
	);
	$rewrite = array(
		'slug'         			=> 'missions/missionaries/agency',
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'query_var'         		=> 'missionagency',
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'missionagency' taxonomy. */
	register_taxonomy( 'missionagency', array( 'missionaries' ), $args );
}

// Add custom meta box to mission country taxonomy Edit Term page
// @source: http://pippinsplugins.com/adding-custom-meta-fields-to-taxonomies/
function endvr_taxonomy_add_new_meta_field_missionagency() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[missionagency_website]"><?php _e( 'Mission Agency Website', 'endvr' ); ?></label>
		<input type="text" name="term_meta[missionagency_website]" id="term_meta[missionagency_website]" value="">
		<p class="description"><?php _e( 'Enter the mission agency\'s website URL.','endvr' ); ?></p>
	</div>
<?php
}
add_action( 'missionagency_add_form_fields', 'endvr_taxonomy_add_new_meta_field_missionagency', 10, 2 ); // {$taxonomy_name}_add_form_fields

// Add HTML to Edit Term page + check to see if the meta field has any data saved already, so that we can populate the field on load.
function endvr_taxonomy_edit_meta_field_missionagency($term) {

	// put the term ID into a variable
	$t_id = $term->term_id;

	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "missionagency_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[missionagency_website]"><?php _e( 'Mission Agency Website', 'endvr' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[missionagency_website]" id="term_meta[missionagency_website]" value="<?php echo esc_attr( $term_meta['missionagency_website'] ) ? esc_attr( $term_meta['missionagency_website'] ) : ''; ?>">
			<p class="description"><?php _e( 'Enter the mission agency\'s website URL.','endvr' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'missionagency_edit_form_fields', 'endvr_taxonomy_edit_meta_field_missionagency', 10, 2 ); // {$taxonomy_name}_edit_form_fields

// Save extra taxonomy fields callback function.
function endvr_save_taxonomy_custom_meta_missionagency( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "missionagency_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "missionagency_$t_id", $term_meta );
	}
}
add_action( 'edited_missionagency', 'endvr_save_taxonomy_custom_meta_missionagency', 10, 2 ); // edited_{$taxonomy_name}
add_action( 'create_missionagency', 'endvr_save_taxonomy_custom_meta_missionagency', 10, 2 ); // create_{$taxonomy_name}