<?php
/**
 * Taxonomy ( Register Mission Country )
 *
 * @package  		ChurchAmp_Missions
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/missions
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_missioncountry' );
function endvr_register_tax_missioncountry() {

	$labels = array(
		'name'                       	=> __( 'Mission Countries',                           			'churchamp-missions' ),
		'singular_name'              	=> __( 'Mission Country',                            			'churchamp-missions' ),
		'menu_name'                  	=> __( 'Mission Countries',                           			'churchamp-missions' ),
		'name_admin_bar'             	=> __( 'Mission Countries',                            		'churchamp-missions' ),
		'search_items'               	=> __( 'Search '.'Mission Countries'.'',                    	'churchamp-missions' ),
		'popular_items'              	=> __( 'Popular '.'Mission Countries'.'',                   	'churchamp-missions' ),
		'all_items'                  	=> __( 'All '.'Mission Countries'.'',                       	'churchamp-missions' ),
		'edit_item'                  	=> __( 'Edit '.'Mission Country'.'',                       		'churchamp-missions' ),
		'view_item'                  	=> __( 'View '.'Mission Country'.'',                       		'churchamp-missions' ),
		'update_item'                	=> __( 'Update '.'Mission Country'.'',                     		'churchamp-missions' ),
		'add_new_item'               	=> __( 'Add New '.'Mission Country'.'',                    		'churchamp-missions' ),
		'new_item_name'             	=> __( 'New '.'Mission Country'.' Name',                		'churchamp-missions' ),
		'separate_items_with_commas' 	=> __( 'Separate '.'Mission Countries'.' with Commas',      	'churchamp-missions' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.'Mission Countries'.'',             	'churchamp-missions' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.'Mission Countries'.'',		'churchamp-missions' ),
	);
	/* only 2 caps are needed: 'manage_missions' and 'edit_missions'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_missions',
		'edit_terms'   			=> 'manage_missions',
		'delete_terms' 			=> 'manage_missions',
		'assign_terms' 			=> 'edit_missions',
	);
	$rewrite = array(
		'slug'         			=> 'missions/missionaries/country',
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'query_var'         		=> 'missioncountry',
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'missioncountry' taxonomy. */
	register_taxonomy( 'missioncountry', array( 'missionaries' ), $args );
}